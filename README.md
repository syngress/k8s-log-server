# Kubernetes Network Log Server with Grafana and ELK Stack
![kubeIMG](https://syngress.pl/images/kubernetes_log_server/kubernetes_logo.png)![RubyIMG](https://syngress.pl/images/ruby_logo.png)![elkIMG](https://syngress.pl/images/kubernetes_log_server/elasticsearch_logo.png)


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)  

Description:  

Project includes creation of a test environment (hardware, software), enabling launch of a network log server.  
Hardware part will include parts completion (ssd's, cpu, motherboard, case, memory, psu, fan's).  
Software part will describe application architecture, we will focus on each of its elements considering application implementation.  
At the very end, we will enable generation of logs from OpenWRT router.  

Things you may want to cover for local development:

- Ruby On Rails Framework 6.0.3.2  
- Ruby 2.7.1  
- Redis 6.0.6    
- ELK 7.8.1  
- Sidekiq 6.1.1  
- Minimum of RAM 64GB  
- MongoDB Database 4.2.8  
- Postgres 12.3  
- Kubernetes 1.18.0  
- Docker 19.03.12  

# HARDWARE  

We are considering building a development environment that enables us to launch several services.  
We are **not** building professional multiprocessor server with redundant power supply.  
Our target is to build machine that will enable us to create development environment discussed below.  
We need to focus on computing power (CPU) and amount of memory (RAM).  
If you do not specify a CPU limit for a Container in Kubernetes config, one of these situations applies:  

- Container has no upper bound on the CPU resources it can use. Container could use all of the CPU resources available on the Node where it is running.  
- Container is running in a namespace that has a default CPU limit, and automatically assigned the default limit. Cluster administrators can use a LimitRange to specify a default value for the CPU limit.  

We need processor that will give us a minimum of 6 logical cores in a hardware term that describes the number of independent central processing units in a single computing component and 6 or 12 threads, or thread of execution, in a software term for the basic ordered sequence of instructions that can be passed through or processed by a single CPU logical core.  

Example hardware.  

- Netrack server case mini-ITX/microATX, 482/88, 8/390mm, 2U, 19'
- Netrack sliding rails for RACK 19' server case, depth 25 - 78 cm
- CHF CMR-425 Chieftec CMR-425 slot 5.2inch for 4x2.5inch SATA drives, Hot-Swap
- be quiet! PURE POWER 11 400W PSU
- G.SKILL Aegis 64GB 4x16 3200MHz DDR4 CL16 XMP
- NOCTUA NF-A8 PWM 80MM x2
- Intel Core i5-9600K
- ASUS PRIME H370M-PLUS Motherboard
- Samsung 250GB M.2 PCIe NVMe 970 EVO Plus SSD
- Samsung 1TB 2,5" SATA SSD 870 QVO x4

Size of SSDs drives and their manufacturer are selected according to our needs.  
For system and log storage we will use fast Samsung M.2 PCIe NVMe 970 EVO Plus SSD drive.  
For other data storage we can use 1TB SSD's (x4) packed in a 2.5 disk bay from Chieftec.  
Power supply is 80 PLUS Gold certified PSU from be quiet!, which gives us a bit more security, selected unit gives us a lot of power to meet our needs.  
Centos 8 for operating system (minimal version).  

# KUBERNETES  

A few words about Kubernetes  

Applications running with Kubernetes simply scale by running consecutive containers.  
In this way, the process can keep pace with the increase number of users and the amount of information processed.  
Applications can be scaled up or down using the graphics console or a simple CLI command, or automatically based on the processor load level.  
Kubernetes can be run in the physical infrastructure of our own server room, or on one or more public cloud platforms, or using a hybrid approach - some here and some there.  
This allows you to easily move applications between environments as planned or ad hoc in the event of unexpected load increases in your own server room.  
Kubernetes can place individual containers based on their resource requirements or other critical constraints.  
Kubernetes gradually propagates changes in the application or its configuration from the first container to the next (rollout), while controlling whether it works correctly after the changes.  
In the event of operating errors, it performs an automatic rollback.  
Kubernetes can automatically mount disk volumes, local or networked: NFS, iSCSI, Gluster, Ceph, Cinder, Flocker or offered in the public cloud: GCP, AWS  
Kubernetes restarting containers that stopped working properly.  
When the whole node goes down, it removes the containers that do not respond to the health-check from the list of available resources until they are ready for operation again.  
Defective containers may be replaced with newly launched ones.  
Kubernetes gives containers their own IP addresses and a single DNS name for a set of containers, allowing load-balancing between them.  

Kubernetes can be created based on the 3 most recognizable tools below:

- ***Minikube*** (Single node kubernetes cluster).
- ***Kops*** (Multi node kubernetes setup into AWS).
- ***Kubeadm*** (Multi Node Cluster in our own premises, does not create configuration or dependency configuration at the operating system level).

Our approach configure Kubernetes cluster from scratch using `Kubeadm`, and deploy containerized application to it.  
Our development cluster will contain **1** master node and **2** nodes, so-called workers.  

**MASTER NODE**  
Master Node (server-related node in Kubernetes) is responsible for managing cluster via `kubeadm` or `kubectl`.  

**WORKER NODES**  
Workers, work nodes on which applications and container services will be run.  
Worker will start some work after being assigned to it, even if the master stops running after workers have finished scheduling.  
Cluster capacity can be simply increased by adding new workers.  

# KUBERNETES DEPLOYMENT  

Create 3 virtual machines with the Centos 8, and minimum 3GB of Ram + 2 CPUs for each node.  
You can choose any software you prefer to create vm's, for example `proxmox` or `vmware esxi`.  

https://www.proxmox.com/en/downloads  
https://my.vmware.com/en/web/vmware/evalcenter?p=free-esxi7  

**Node master** (x1) will contain following components:  

**_API Server_** – It provides kubernetes API using Jason / Yaml over http, states of API objects are stored in etcd.  
**_Scheduler_** – It is a program on master node which performs the scheduling tasks like launching containers in worker nodes based on resource availability.  
**_Controller Manager_**  – Main Job of Controller manager is to monitor replication controllers and create pods to maintain desired state.  
**_etcd_** – It is a Key value pair data base. It stores configuration data of cluster and cluster state.  
**_Kubectl utility_** – It is a command line utility which connects to API Server on port 6443. It is used by administrators to create pods, services etc.   

**Worker nodes** (x2) will contain the following components:  

**_Kubelet_** – It is an agent which runs on every worker node, it connects to docker  and takes care of creating, starting, deleting containers.  
**_Kube-Proxy_** – It routes the traffic to appropriate containers based on ip address and port number of the incoming request. In other words we can say it is used for port translation.  
**_Pod_** – Pod can be defined as a multi-tier or group of containers that are deployed on a single worker node or docker host.  

Install system on each of the virtual machines (node's).  
For each node separately configure static network connection with different assigned static IP address.  

![staticIP](https://www.iplocation.net/assets/images/blog/featured/static-vs-dynamic-ip-address.jpg)  

`/etc/sysconfig/network-scripts/ifcfg-xxx`  

```code
TYPE=Ethernet  
PROXY_METHOD=none  
BROWSER_ONLY=no  
BOOTPROTO=static  
DEFROUTE=yes  
IPV4_FAILURE_FATAL=no  
IPV6INIT=yes  
IPV6_AUTOCONF=yes  
IPV6_DEFROUTE=yes  
IPV6_FAILURE_FATAL=no  
IPV6_ADDR_GEN_MODE=stable-privacy  
NAME=ens192  
UUID=e4003be3-1a8f-4d4d-a646-c50b0b147625  
DEVICE=ens192  
ONBOOT=yes  
IPADDR=10.0.1.xxx  
NETMASK=255.255.255.0  
GATEWAY=10.0.1.1  
DNS1=10.0.1.1  
DNS2=8.8.8.8  
ZONE=public  
```

Restart each node, after rebooting update system:  

```code
yum update
```

Add entries to ***/etc/hosts*** on all nodes (master, worke's), based on what you provided in the static configuration of network interfaces.  

```code
10.0.1.10 node-master
10.0.1.11 worker-node1
10.0.1.12 worker-node2
```

Each node (master, worker's) should have swap turned off.  
On each node, comment on SWAP in ```/etc/fstab```  

Log in as root to the **master** node.  
Set node hostname and disable selinux security.  

```code
[master]# hostnamectl set-hostname 'node-master'
[master]# exec bash
[master]# setenforce 0
[master]# sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

Set firewall rules.  

```code
[master]# firewall-cmd --permanent --add-port=6443/tcp
[master]# firewall-cmd --permanent --add-port=2379-2380/tcp
[master]# firewall-cmd --permanent --add-port=10250/tcp
[master]# firewall-cmd --permanent --add-port=10251/tcp
[master]# firewall-cmd --permanent --add-port=10252/tcp
[master]# firewall-cmd --permanent --add-port=10255/tcp
[master]# firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 4 -i docker0 -j ACCEPT
[master]# firewall-cmd --reload
[master]# modprobe br_netfilter
[master]# echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```

Install docker.  Download latest available version.  

```code
[master]# dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
[master]# wget https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
[master]# yum localinstall containerd.io-1.2.6-3.3.el7.x86_64.rpm
[master]# dnf install docker-ce docker-ce-cli
[worker]# usermod -aG docker root
```

Install docker-compose.  

```code
[worker]# curl -L "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
[worker]# chmod +x /usr/local/bin/docker-compose
[worker]# docker-compose -v
```

Configure Kubernetes repository.  

```code
[master]# nano /etc/yum.repos.d/kubernetes.repo

[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
```

Update node with docker.  

```code
[master]# yum update
```

Install kubeadm.  

```code
[master]# yum install kubeadm -y
```

Our docker has no DNS, we need to do a workaround and perform it on all nodes (master, worker's):  

```code
[master/node]# sudo nano /etc/firewalld/firewalld.conf

# change

FirewallBackend=nftables

# to

FirewallBackend=iptables

[master/node]# sudo systemctl restart firewalld.service
```

Run docker and kubelet services.  

```code
[master]# systemctl restart docker && systemctl enable docker
[master]# systemctl restart kubelet && systemctl enable kubelet
```  

Initialize **master** kubernetes node.  

```code
[master]# kubeadm init
```  

In response, CLI return something similar to:  

```code
Your Kubernetes control-plane has initialized successfully!
To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 10.0.1.10:6443 --token ddv5tz.xsdx26uo0giwsqdy \
    --discovery-token-ca-cert-hash sha256:3524c05ba77ed58d02cd5f92042e52f75d98013c7be64a8f484c2b2b0ed99c0a

[root@kube-master ~]#
```  

Node master has generated token `--token ddv5tz.xsdx26uo0giwsqdy` which is temporary and will become expired after some short time.  
We'll deal with it later.  
Above output informs us about the **correct initialization** of the node master.  
Now follow the commands below to use node as root.  

```code
[master]# mkdir -p $HOME/.kube
[master]# cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
[master]# chown $(id -u):$(id -g) $HOME/.kube/config
```

Check status of the K8S cluster nodes.  

```code
[master]# kubectl get nodes
NAME            STATUS      ROLES    AGE     VERSION
node-master     NotReady    master   1h31m   v1.18.3
```

Note that node master is in the **not ready** status.  
Check POD statuses.  

```code
[master]# kubectl get pods --all-namespaces

NAMESPACE     NAME                                  READY   STATUS    RESTARTS   AGE
kube-system   coredns-66bff467f8-2vmtn              1/1     Pending   0          3h33m
kube-system   coredns-66bff467f8-495cf              1/1     Pending   0          3h33m
kube-system   etcd-kube-master                      1/1     Running   0          3h33m
kube-system   kube-apiserver-kube-master            1/1     Running   0          3h33m
kube-system   kube-controller-manager-kube-master   1/1     Running   0          3h33m
kube-system   kube-proxy-62jf2                      1/1     Running   0          3h7m
kube-system   kube-proxy-65qc8                      1/1     Running   0          3h33m
kube-system   kube-proxy-6qmp8                      1/1     Running   0          3h8m
kube-system   kube-scheduler-kube-master            1/1     Running   0          3h33m
kube-system   weave-net-hrnhv                       2/2     Running   0          3h8m
kube-system   weave-net-q6qsk                       2/2     Running   0          3h7m
kube-system   weave-net-zpckk                       2/2     Running   0          3h30m
```

Note that pod coredns is in the **pending** status.  
In order to get status ready for master node and kube-dns to status running, we need to deploy network POD, so containers will be able to communicate with each other.  
POD Network is an overlay network between worker nodes.  
To embed network, run the following commands:  

```code
[master]# export kubever=$(kubectl version | base64 | tr -d '\n')
[master]# kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"

serviceaccount "weave-net" created
clusterrole "weave-net" created
clusterrolebinding "weave-net" created
daemonset "weave-net" created
```

Now wait couple minutes, your node will prepare everything for work.  
Check statuses.  

```code
[master]# kubectl get nodes

NAME            STATUS      ROLES    AGE     VERSION
node-master     Ready    master   1h31m   v1.18.3

[master]# kubectl get pods --all-namespaces

NAMESPACE     NAME                                  READY   STATUS    RESTARTS   AGE
kube-system   coredns-66bff467f8-2vmtn              1/1     Running   0          3h33m
kube-system   coredns-66bff467f8-495cf              1/1     Running   0          3h33m
kube-system   etcd-kube-master                      1/1     Running   0          3h33m
kube-system   kube-apiserver-kube-master            1/1     Running   0          3h33m
kube-system   kube-controller-manager-kube-master   1/1     Running   0          3h33m
kube-system   kube-proxy-62jf2                      1/1     Running   0          3h7m
kube-system   kube-proxy-65qc8                      1/1     Running   0          3h33m
kube-system   kube-proxy-6qmp8                      1/1     Running   0          3h8m
kube-system   kube-scheduler-kube-master            1/1     Running   0          3h33m
kube-system   weave-net-hrnhv                       2/2     Running   0          3h8m
kube-system   weave-net-q6qsk                       2/2     Running   0          3h7m
kube-system   weave-net-zpckk                       2/2     Running   0          3h30m
```

Now generate new token that will not expire.  

```code
[master]# kubeadm token create --ttl 0
```

You can always view the list of available tokens.  

```code
[master]# kubeadm token list

TOKEN                     TTL         EXPIRES   USAGES                   DESCRIPTION   EXTRA GROUPS
v5lozd.04u0ke3c0x0whjn5   <forever>   <never>   authentication,signing   <none>        system:bootstrappers:kubeadm:default-node-token
```

Sometimes an error will appear informing about problems with getting statistics from containers, we can eliminate it by adding two extra arguments to the kubelet configuration file.  

```code
[master]# nano /etc/sysconfig/kubelet

KUBELET_EXTRA_ARGS=--runtime-cgroups=/systemd/system.slice --kubelet-cgroups=/systemd/system.slice
```  

After making configuration changes, restart kubelet, wait till node master is ready for operation again.  

```code
[master]# systemctl restart kubelet

[master]# kubectl get nodes
NAME          STATUS   ROLES    AGE     VERSION
kube-master   Ready    master   4d19h   v1.18.3
```  

Now add new workers.  

# WORKER NODES

Run following commands as root on each worker, remember to assign each worker a different hostname.  

```code
[worker]# hostnamectl set-hostname 'worker-node1'
[worker]# exec bash
[worker]# setenforce 0
[worker]# sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
[worker]# firewall-cmd --permanent --add-port=6443/tcp
[worker]# firewall-cmd --permanent --add-port=2379-2380/tcp
[worker]# firewall-cmd --permanent --add-port=10250/tcp
[worker]# firewall-cmd --permanent --add-port=10251/tcp
[worker]# firewall-cmd --permanent --add-port=10252/tcp
[worker]# firewall-cmd --permanent --add-port=10255/tcp
[master]# firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 4 -i docker0 -j ACCEPT
[worker]# firewall-cmd --reload
[worker]# modprobe br_netfilter
[worker]# echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```

Setup kubernetes repository for both workers.  

```code
[worker]# nano /etc/yum.repos.d/kubernetes.repo

[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

[worker]# yum update --nobest
```

Install docker and docker-compose on both workers.  

```code
[worker]# dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
[worker]# dnf install docker-ce —nobest
[worker]# yum install -y docker-ce-cli-1:19.03.8-3.el7
[worker]# usermod -aG docker root
[worker]# curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
[worker]# chmod +x /usr/local/bin/docker-compose
[worker]# docker-compose -v
```

Docker has no DNS, we have to workaround.  

```code
[worker]# sudo nano /etc/firewalld/firewalld.conf

# change

FirewallBackend=nftables

# to

FirewallBackend=iptables

[worker]# sudo systemctl restart firewalld.service
```

Install kubeadm on both workers.  

```code
[worker]# yum install kubeadm
```

Run docker service on both workers.  

```code
[worker]# systemctl restart docker && systemctl enable docker && systemctl enable kubelet
```

Join each worker to the master node, for this purpose you will use token that you received after initializing master or perm token that you generated manually.  

```code
[worker]# kubeadm join --discovery-token-unsafe-skip-ca-verification --v=2 --token ddv5tz.xsdx26uo0giwsqdy 10.0.1.10:6443
```

For each node CLI should return `Node join complete` in response.  
Wait 5 minutes, system automatically configure attached nodes.  
Verify configurations from **master** node.  

```code
[master]# kubectl get nodes
NAME          STATUS   ROLES    AGE     VERSION
kube-master   Ready    master   4h      v1.18.3
kube-node1    Ready    <none>   3h34m   v1.18.3
kube-node2    Ready    <none>   3h34m   v1.18.3
```

# REMOVE UNUSED NODES

If node is no longer needed, you can delete it with command.  

```code
# kubectl cordon <node-name>
# kubectl drain <node-name> --ignore-daemonsets --delete-local-data
# kubectl delete node <node-name>
```

# LABEL NODES

You can label your node's with a command:  

```code
# kubectl label node <node name> node-role.kubernetes.io/worker=worker
```

# K8S SIMPLE DEBUG

If one of the worker nodes is unable to connect to the master node, and node status is not ready, we can check the cause of the problem by issuing this command:  

```code
[master]# kubectl describe nodes
```

for more information, send:

```code
[master]# journalctl -u kubelet
```

# [MASTER NODE] CONNECTION REFUSED AFTER RESTART ?  

It may happen that after restarting k8s cluster, you will not be able to issue any `kubectl` command and receive error below:  

```code
The connection to the server localhost:6443 was refused - did you specify the right host or port?
```  

It happens because Kubernetes control-plane did not initialize correctly after restart.  
We are not able to re-initialize at this point because all configuration files generated by cluster initialization still exist.  
The fastest way to fix this issue in a test environment is to reset control-plane.  

First of all, check cluster configuration. Is it visible and correctly read by cluster ?  

```code
kubectl config view
```  

If you receive something similar to:  

```code
apiVersion: v1
clusters: null
contexts: null
current-context: ""
kind: Config
preferences: {}
users: null
```  

It means that configuration was not found, execute command below and check configuration again.   

```code
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```  

If you are still unable to execute any `kubectl` commands, reset environment.  
Don't paste everything, perform one command at a time.  

```code
kubeadm reset
rm $HOME/.kube/config file
kubeadm init
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export kubever=$(kubectl version | base64 | tr -d '\n')
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"
```

Now wait for a while and check if all nodes have `ready` status.  

```code
kubectl get nodes
```

Also check pods, they should all be in `running` status.  
If the statuses are different, wait moment longer and check again.  
If everything is ok, generate new perm token and connect workers again. Remember to `kubeadm reset` on each worker.     

```code
kubeadm token create --ttl 0
```

# POSTGRES POD  

We will create new pod serving postgres database.  
Assuming all nodes are in ready status, and all pods are running, go to the `k8s/postgres_pod` directory, and find `postgres.yml` file.  
Its structure may seem complex, but for someone who has worked with docker, it should be easier to understand its contents.  
The `postgres.yml` file structure is divided into 3 parts:  

- ConfigMap  
- Deployment  
- Service  

**ConfigMaps** allow you to decouple configuration artifacts from image content to keep containerized applications portable.  
**Service** is an abstract way to expose application running on a set of Pods as a network service.  
**Deployment** provides declarative updates for Pods and ReplicaSets.  

You can read more about configuration files on the web.  
Copy `postgres.yml` configuration file to the `~/.kube/postgres/postgres.yml` directory of the master node.  
Initialize postgres pod in cluster with the command below:  

```code
kubectl create -f ~/.kube/postgres/postgres.yml
```

In response system should return:  

```code
configmap/postgres-config created
deployment.apps/postgres created
service/postgres created
```

Wait for a while, cluster will initialize postgres service by downloading and run docker image.  
Check if the pod has been run correctly.  

```code
kubectl get pods
```

In response system should return:  

```code
NAME                        READY   STATUS    RESTARTS   AGE
postgres-6887d9d48c-658rz   1/1     Running   0          20s
postgres-6887d9d48c-bmb75   1/1     Running   0          20s
```

Check postgres service:  

```code
kubectl get svc postgres
```

In response system should return:  

```code
NAME       TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
postgres   LoadBalancer   10.111.20.163   10.1.3.120    5432:30507/TCP   11m
```

Now we know that our service is working properly.  
Everyone who had contact with docker knows how docker manages network.  
You must remember that pod runs the image, but manages network itself.  
Service runs on port 5432, but we can't get to it directly from `master node` by enter:  

```code
psql -h localhost -U postgres --password -p 5432 postgres
```

It just won't work !  
Alternatively, you can enter directly from `master node` to the postgres pod service and run the command again giving the cluster IP address:  

```code
kubectl exec -it postgres-6887d9d48c-d5t9v -- psql -h 10.111.20.163 -U postgres --password -p 5432 postgres
```

But hey! Let's try to connect to the postgres service from `another worker` which belongs to the cluster :)  

```code
psql -h 10.111.20.163 -U postgres --password -p 5432 postgres
```

And `voilà` in response we will receive:  

```code
Password for user postgres:
psql (10.15, server 11.11 (Debian 11.11-1.pgdg90+1))
WARNING: psql major version 10, server major version 11.
         Some psql features might not work.
Type "help" for help.

postgres=#
```  

Now you are wondering why I can't get directly to the pod service from the `master` node in the same way as from the `worker` node ?
Well, the answer is because k8s master node cannot see `10.111.20.163` IP address, but sees the pod `postgres-6887d9d48c-d5t9v` service.  
Situation is different with workers, when worker node tries to establish connection, cluster will automatically perform network translations from the entered IP address.  
Now let's try expose service outside the cluster ..  
The `hostNetwork: true` setting is responsible for expose service outside the k8s cluster.  
When pod is configured with `hostNetwork: true`, applications running in such a pod can directly see network interfaces of the host machine where the pod was started.  
Application that is configured to listen on all network interfaces will in turn be accessible on all network interfaces of the host machine.  
Now try to access the postgres pod service from another machine that works on the same network segment, but `is not` a cluster worker !  

```code
kubectl describe pod  postgres
```

In response, cluster returns information on which of the k8s cluster workers postgres service was deployed.  

```code
Name:         postgres-9b4f786dd-52vj6
Namespace:    default
Priority:     0
Node:         k8node1/10.0.1.102
Start Time:   Sun, 28 Feb 2021 15:24:45 +0100
Labels:       app=postgres
              pod-template-hash=9b4f786dd
Annotations:  <none>
Status:       Running
IP:           10.0.1.102
IPs:
  IP:           10.0.1.102
Controlled By:  ReplicaSet/postgres-9b4f786dd
```

We got a clear answer in the feedback.  
Postgres pod service works on a worker with the IP address `k8node1 / 10.0.1.102`.  
So now we know 2 important things, connect to IP `10.0.1.102` on port `5432` reach postgres database from outside k8s cluster :)  

```code
psql -h 10.0.1.102 -U postgres --password -p 5432 postgres
```

And we should receive as answer:  

```code
Password for user postgres:
psql (10.15, server 13.2 (Debian 13.2-1.pgdg100+1))
WARNING: psql major version 10, server major version 13.
         Some psql features might not work.
Type "help" for help.

postgres=#
```

![kube_pgIMG](https://syngress.pl/images/kubernetes_log_server/k8s_postgres.png)


# OPENWRT LOGS  

OpenWrt Project is a Linux operating system targeting embedded devices, mostly routers.    
We are going to offload connection information from OpenWRT router to a central syslog server.  
We will use session length and GeoIP metadata along with syslog-ng.  
According to [this](https://www.kernel.org/doc/ols/2005/ols2005v2-pages-273-278.pdf) document, ``nf_conntrack`` in Linux kernel (netfilter) keeps track of every connections throughout their lifetime (even UDP which is stateless).  
We can use [ulogD](https://www.netfilter.org/projects/ulogd/index.html) to get that data off the OpenWRT router.  
Ulogd is capable to log connection tracking data to local syslog and OpenWRT uses logread to send syslogs remotely.  
You can read more about ulogD [here](https://home.regit.org/2014/02/logging-connection-tracking-event-with-ulogd/)  

First, install packages in your OpenWRT router.  

```code
# opkg install ulogd ulogd-mod-nfct ulogd-mod-syslog ulogd-mod-extra
```

Now go to your router ulogd configuration file placed in `etc/ulogd.conf`  
Copy paste configuration located in this repository `openwrt/etc/ulogd.conf` to your router ulogd configuration file.  
Configuration can be verified by running ulogd in verbose mode, for example:  

```code
# ulogd -v

Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `NFCT'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `IFINDEX'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `IP2STR'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `PRINTPKT'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `HWHDR'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `PRINTFLOW'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `LOGEMU'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `SYSLOG'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `GPRINT'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `BASE'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:407 registering plugin `GRAPHITE'
Sat Aug 15 18:18:58 2020 <5> ulogd.c:980 building new pluginstance stack: 'ct1:NFCT,ip2str1:IP2STR,print1:PRINTFLOW,sys1:SYSLOG'
Sat Aug 15 18:18:58 2020 <5> ulogd_inpflow_NFCT.c:1399 NFCT plugin working in event mode
```  

If everything is fine and ulogd doesn't return any errors on startup, start/enable service and restart your OpenWRT router.  

```code
# /etc/init.d/ulogd enable
# /etc/init.d/ulogd start
# reboot
```  

Now all you need to do is setup OpenWRT router to send logs to the remote log server.  
You can do it in `system/system properties` of OpenWRT GUI or form the CLI.  

![opewrt_guiIMG](https://syngress.pl/images/kubernetes_log_server/openwrt_logging.png)

Set the following values:  

`System Log Buffer Size:` **128**  
`External System Log Server:` **IP of your LogServer NIC**  
`External System Log Server Port:` **601**  
`External System Log Server Protocol:` **TCP**

Save and reboot your OpenWRT router.  

# SYSLOG  

At the moment, we will omit the container placement in K8S pod.  
First we will check whether communication between services is working properly.  
Then we will verify that syslog is collecting logs correctly.  

We have defined the syslog service in the `docker-compose.yml` file.  

```code
syslog:
    image: balabit/syslog-ng:latest
    hostname: syslogng
    command: "--no-caps"
    volumes:
      - /mnt/nvm1/logs:/var/log/syslog-ng
      - /mnt/nvm1/socket:/var/run/syslog-ng
      - /mnt/nvm1/syslog/config:/etc/syslog-ng
    ports:
      - "514:514/udp"
      - "601:601/tcp"
      - "6514:6514"
    restart: always
```  

Service configuration can be found in the `volumes` section.  
Note the directory `/mnt/nvm1` it's my location, yours can be anything you like, change accordingly.  
You will find syslog configuration files in this repository, under same directory name.  

Syslog is a protocol which is defined in RFC 5424 and RFC 3164. Port number is defined as 514 with UDP protocol for syslog services.  
There is also a recommendation about source port to be UDP 514 too.  
This port number is also registered by IANA to the syslog protocol which means other applications can not use 514 as official default port.  

UDP protocol supplies minimized transmission delay by omitting the connection setup process, flow control, and retransmission, unfortunately UDP protocol is unreliable.  
TCP protocol provides retransmission in the event of network failure, so we will use it for communication.  
TCP uses SNIP for sending monitoring probes to check the connectivity and then sends the logs over NSIP.  

**SYSLOG CONFIGURATION FILE**

`/mnt/nvm1/syslog/config/syslog-ng.conf`  

Source driver called `s_network` is listen for incoming logs on the network with your NIC IP address on TCP port 601.  
Destination driver called `d_network` is responsible for storing received logs on the file system.  
We are connecting source and destination drivers to each other in a log path called `log`  

Macros like `$S_YEAR` or `$S_DAY` enables you to use values from the parsed log messages.  
In our example it creates log file names like:  

`/var/log/network/SOME_ROUTER_NAME_OR_IP/2020.05.20/messages`  

Syslog configuration allow to translate outbound IP addresses with GeoLite2-City database.  
Create your own account at: https://www.maxmind.com/en/account/login and download latest GeoLite2-City database.  
Move the downloaded file to the `/etc/syslog-ng/` directory, that will be your mounted volume, for example `/mnt/nvm1/syslog/config`  

```code
# Sample part of syslog the configuration file
geoip2(
  "${outbound.destination.ip}",
   prefix( "geo." )
   database( "/etc/syslog-ng/GeoLite2-City.mmdb" )
);
```  

Open ports on system where the syslog service is running.  

```code  
firewall-cmd --zone=public --add-port=514/udp --permanent
firewall-cmd --zone=public --add-port=601/tcp --permanent
firewall-cmd --zone=public --add-port=6514/tcp --permanent
firewall-cmd --reload
```  

**ELASTICSEARCH**  

We will use Elasticsearch to store the data.  
For this purpose, we will use elastic cluster consisting of 3 nodes.  
We defined 2 destinations in the syslog configuration file.  

First destination writes data to a file:
```code
# Sample part of syslog the configuration file
destination d_network_conntrack{
    file(
        "/var/log/network/$HOST/$S_YEAR.$S_MONTH.$S_DAY/messages.json"
        template("$(format_json --pair host.name=$HOST --pair host.ip=$SOURCEIP --pair @timestamp=$ISODATE --pair message=$MESSAGE --pair tags=openwrt --pair ecs.version=1.0.0 --key host.* --key event.* --key inbound.s* --key inbound.d* --key outbound.s* --key outbound.d* --key network.*)\n")
        create-dirs(yes)
    );
};
```  

Secod destination pushes data to elasticsearch.  
```code
# Sample part of syslog the configuration file
destination d_elastic_network {
    http(url("http://`elastic_host`/_bulk")
        method("POST")
        batch-lines(3)
        workers(4)
        headers("Content-Type: application/x-ndjson")
        body-suffix("\n")
        body(
            "{ \"index\":{ \"_index\": \"network-${S_YEAR}-${S_MONTH}-${S_DAY}\" } }\n$(format_json --pair host.name=$HOST --pair host.ip=$SOURCEIP --pair @timestamp=$ISODATE --pair message=$MESSAGE --pair tags=openwrt --pair ecs.version=1.0.0 --key host.* --key event.* --key inbound.s* --key inbound.d* --key outbound.s* --key outbound.d* --key network.*)\n")
        disk-buffer(
            mem-buf-length(10000)
            disk-buf-size(10000000)
            reliable(no)
        )
        persist-name("network")
    );
};  
```  

Before data goes to elastic, we need to define an index template.  
You can read more about elasticsearch index template's [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/index-templates.html).  
Just copy the contents of this file `mnt/nvm1/syslog/config/elastic_template`  
In the `docker-compose` file, services have physical paths configured, verify each service path and adjust according to your requirements.  
Run `docker-compose up` and wait till ELK stack is initialized.  
Paste content to your CLI and run the PUT command.  

**VERIFICATION**  

First, we will check if syslog is collecting data.  

Check if syslog container is up and running  
```code
[test@logserver ~]$ docker ps | grep syslog
999998a0701a balabit/syslog-ng:latest "/usr/sbin/syslog-ng…" 5 weeks ago Up 3 hours (healthy) 0.0.0.0:601->601/tcp, 0.0.0.0:514->514/udp, 0.0.0.0:6514->6514/tcp test_syslog_1
```  

Syslog configurations put logs in the `/mnt/nvm1/logs` directory. Your path may be different.  
```code
volumes:
      - /mnt/nvm1/logs:/var/log/syslog-ng
```  

If everything is configured correctly, you should see the directory `network` after the `/mnt/nvm1/logs` path.  

The `network` folder should contain logs from each day in a separate directory, for example:  

```code
2020.08.30  2020.09.01  2020.09.03  2020.09.05  2020.09.07  2020.09.09  2020.09.11  2020.09.13  2020.09.15  2020.09.17  2020.09.19  2020.09.21  2020.09.23  2020.09.25
2020.08.31  2020.09.02  2020.09.04  2020.09.06  2020.09.08  2020.09.10  2020.09.12  2020.09.14  2020.09.16  2020.09.18  2020.09.20  2020.09.22  2020.09.24  2020.09.26
```  

Each of directory should contain `message` file with example content:  

```code
Sep 26 19:06:59 192.168.2.1 ulogd[1863]: [NEW] ORIG: SRC=192.168.2.111 DST=17.248.147.15 PROTO=TCP SPT=59679 DPT=443 PKTS=0 BYTES=0 , REPLY: SRC=17.248.147.15 DST=192.168.1.102 PROTO=TCP SPT=443 DPT=59679 PKTS=0 BYTES=0
```  

Now let's take a look what do we have in elasticsearch.  
Try to enter this `http://ip_of_your_log_server:9200/` address in your browser. In response You should see something like:  

```code
{
name: "elasticsearch",
cluster_name: "es-docker-cluster",
cluster_uuid: "morZTl8JTDKNWz6WvTGEPw",
version: {
number: "7.8.1",
build_flavor: "default",
build_type: "docker",
build_hash: "b5ca9c58fb664ca8bf9e4057fc229b3396bf3a89",
build_date: "2020-07-21T16:40:44.668009Z",
build_snapshot: false,
lucene_version: "8.5.1",
minimum_wire_compatibility_version: "6.8.0",
minimum_index_compatibility_version: "6.0.0-beta1"
},
tagline: "You Know, for Search"
}
```  

This response will inform that the service is up and running.  
Try to create index pattern, go to `http://ip_of_your_log_server:5601/app/kibana#/management/kibana/index_pattern?_g=()` Kibana address.  
In index pattern field enter `network-*` system should find index automatically, on one condition, syslog must send at least one message beforehand to elasticsearch !  
Complete the index setup and go to Discover tab, logs should run down.  
Remember that we only log established connections, for performance reasons, connections that have not been established are not logged.  

Sample screen with logged connection from elasticsearch database:  

![elasticLog](https://syngress.pl/images/kubernetes_log_server/elasticLog.png)

**NETWORK TRAFFIC ANALYSIS**  

Kibana allows us to generate convenient data sets with filters.  
Narrowing results to one day, we choose network from which connection comes out, source ip, source port, destination ip, destination port.  
Note that only established and related connections are logged !  

![connectionAnalysis](https://syngress.pl/images/kubernetes_log_server/connection_analysis.png)  

QUERY:    
```code
outbound.destination.port : 80
```  

Let's check connections on port 80.  
There were 1,527 connections to port 80 within 24 hours.  
47.254.187.25 (Aliexpress) is returned with the highest number of results, source IP points to the wifi gateway, so probably my client have a mobile devices with Aliexpress application installed ;)  
We know that browser make a GET request to port 80, let's check more details with BurpSuite.  

![connectionAnalysisBs](https://syngress.pl/images/kubernetes_log_server/connection_analysis_bs.png)  

The response from the server suggests connection to some API service.  
We can use HEAD instead GET to omit response data.  
We don't have to focus on the response details for now, server answer confirms our conviction that connection can be made by application installed on some device with wireless access.  
We will not be looking at the wireless router logs at this stage, it would be too easy :)  
We will perform a deep packet inspect using Wireshark.  
After obtaining appropriate consent from the client, connect to the client's wireless network and make a GET call on port 80.  

![connectionAnalysisWsh1](https://syngress.pl/images/kubernetes_log_server/connection_analysis_wsh_1.png)  

In response, we get a lot of information.  
Source IP points to my device that connects to the given IP address through the wireless gateway using HTTP protocol.  

![connectionAnalysisWsh2](https://syngress.pl/images/kubernetes_log_server/connection_analysis_wsh_2.png)  

First line contain (method, URL address, protocol version), there must be exactly two spaces between them !!  

```code
GET / HTTP/1.1\r\n
```  

Second line contain header with header name: `host`  

```code
Host: 47.254.187.25\r\n
```  

Low level communication look's like this:  

![connectionAnalysisWsh3](https://syngress.pl/images/kubernetes_log_server/connection_analysis_wsh_3.png)  

String `0d0a` is the CRLF line separator in the HTTP protocol.  
CRLF is a two-character string with ASCII values of 13 and 10, signifying the end of the current line of text.  
Sometimes HTTP request is sent with only one CRLF line break character, after the headers.  
Such request is invalid and usually results in the HTTP server waiting for the CRLF character.  

Now let's check the response.  

![connectionAnalysisWsh4](https://syngress.pl/images/kubernetes_log_server/connection_analysis_wsh_4.png)  

The first line tells us that the server returned 403, server understood the request but refuses to authorize it.  
This tells us that access to resources is protected by host for clients trying to establish connection to the selected resource from Internet.  
In addition, we received information about what server handled our request, in this case it was `AliyunOSS`.  
ALIYUN OSS Bucket is used to create an Alibaba Cloud Object Storage Service (OSS) that enables to store, back up, and archive large amounts of data in the cloud.  
A growing belief that we were dealing with calls made by mobile applications ;)  

![connectionAnalysisWsh5](https://syngress.pl/images/kubernetes_log_server/connection_analysis_wsh_5.png)  

Note the CRLF character after the headers.  
So we have one CRLF after the HOST header and another CRLF after all headers, what is HTTP protocol standard.  

Looking at customer logs from the wireless gateway device, it was found that the calls were actually made from the phone on which the AliExpress application was installed :).  
This way we can assemble, view and analyze network traffic in a completely basic way!  

**APPLICATION ARCHITECTURE DIAGRAM**  

![App Architecture Diagram](https://syngress.pl/images/kubernetes_log_server/k8s_log_server_main_comm.png)

**GRAFANA EXAMPLE SCREEN**

Try Grafana dashboard configuration file `grafana.json`, sample screen below.  

![Grafana](https://syngress.pl/images/kubernetes_log_server/grafana.png)

# LOG ANALYZER MICROSERVICE  

https://bitbucket.org/syngress/k8s-log-analyzer/src/master/

# CSV PARSER MICROSERVICE

https://bitbucket.org/syngress/k8s-csv-parser/src/master/

**Free Software, Hell Yeah!**  

**THIS PROJECT IS STILL UNDER CONSTRUCTION**
