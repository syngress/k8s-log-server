# frozen_string_literal: true

# config/initializers/version.rb
module FoodcareAPI
  ORIGIN_NAME = 'log_analyzer'
  VERSION = '1.0.0'

  def self.info
    { version: VERSION }
  end
end
