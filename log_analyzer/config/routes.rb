# frozen_string_literal: true

Rails.application.routes.draw do
  Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]
  Sidekiq::Web.set :sessions, Rails.application.config.session_options
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == Settings.basic_auth.username && password == Settings.basic_auth.password
  end
  mount Sidekiq::Web => '/sidekiq'
  resources :whois_reports, only: %i[index show create], shallow: true
  resources :destination_addresses, only: %i[index show create], shallow: true
end
