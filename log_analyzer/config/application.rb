# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_mailbox/engine'
require 'action_text/engine'
require 'action_view/railtie'
require 'action_cable/engine'
# require "sprockets/railtie"
require 'rails/test_unit/railtie'
require_relative '../lib/middleware/setup_request_id'
require_relative '../lib/middleware/catch_json_parse_errors'
require_relative '../lib/middleware/set_locale'

Bundler.require(*Rails.groups)

module LogAnalyzer
  # config/application.rb
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0
    config.i18n.available_locales = %i[en en_GB pl_PL]
    config.i18n.default_locale = Settings.i18n.default_locale
    config.i18n.enforce_available_locales = false
    config.i18n.fallbacks = Settings.i18n.fallbacks
    config.api_only = true

    config.exceptions_app = ->(env) { MiddlewareErrorsController.action(:show).call(env) }
    config.time_zone = 'Warsaw'

    config.middleware.use ActionDispatch::Session::CookieStore, key: '_log_analyzer_session'
    config.middleware.insert_after Rack::Head, Middleware::SetupRequestId
    config.middleware.insert_before Middleware::SetupRequestId, Middleware::CatchJsonParseErrors
    config.middleware.insert_before Middleware::CatchJsonParseErrors, Middleware::SetLocale
    config.middleware.use ActionDispatch::Session::CookieStore, key: '_log_analyzer_session'
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: %i[get post delete put patch options head]
      end
    end
  end
end
