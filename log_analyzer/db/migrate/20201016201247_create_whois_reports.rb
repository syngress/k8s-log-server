# frozen_string_literal: true

# db/migrate/20201016201247_create_whois_reports.rb
class CreateWhoisReports < ActiveRecord::Migration[6.0]
  def change
    create_table :whois_reports do |t|
      t.string :log_date
      t.integer :log_lines, default: 0
      t.string :execution_time
      t.string :report_file_name
      t.string :report_file_path

      t.timestamps
    end
  end
end
