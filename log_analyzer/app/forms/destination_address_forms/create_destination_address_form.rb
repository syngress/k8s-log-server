# frozen_string_literal: true

# app/forms/destination_address_forms
module DestinationAddressForms
  # app/forms/destination_address_forms/create_destination_address_form.rb
  class CreateDestinationAddressForm < Dry::Struct
    # dry types
    module Types
      include Dry::Types(default: :nominal)
    end

    attribute :destination_ip, Types::String

    def errors?
      process_error if schema.errors.present?
    end

    def valid?
      schema.errors.blank?
    end

    def schema
      @schema ||= DestinationAddressContracts::DestinationAddressContract.new.call(to_hash)
    end

    def process_error
      return unless schema.errors.messages.first.predicate

      result_data = schema.errors.to_h
      raise ::Error::InvalidParameter.new result_data.keys.first.to_s, value: schema[:destination_ip], error: result_data.values.flatten.first
    end
  end
end
