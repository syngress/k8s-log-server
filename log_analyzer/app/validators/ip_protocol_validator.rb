# frozen_string_literal: true

require 'ipaddr'

# log_analyzer/app/validators/ip_protocol_validator.rb
module IpProtocolValidator
  def self.v4?(ip_address)
    create(ip_address).ipv4?
  end

  def self.v6?(ip_address)
    create(ip_address).ipv6?
  end

  def self.private_ip?(ip_address)
    create(ip_address).private?
  end

  def self.create(ip_address)
    IPAddr.new ip_address
  end
end
