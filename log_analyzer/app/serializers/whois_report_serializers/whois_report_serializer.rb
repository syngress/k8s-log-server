# frozen_string_literal: true

module WhoisReportSerializers
  # log_analyzer/app/serializers/whois_report_serializers/whois_report_serializer.rb
  class WhoisReportSerializer
    include JSONAPI::Serializer
    # cache_options enabled: true, cache_length: 12.hour

    attributes :id, :log_date, :log_lines, :execution_time, :report_file_name, :report_file_path, :created_at, :updated_at
  end
end
