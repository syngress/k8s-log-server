# frozen_string_literal: true

module WhoisReportServices
  # log_analyzer/app/services/whois_report_services/get_whois_report_service.rb
  class GetWhoisReportService
    include Service

    def initialize(attrs = {})
      @sort_by = attrs.fetch(:sort_by)
      @sort_order = attrs.fetch(:sort_order)
      @page = attrs.fetch(:page)
      @per_page = attrs.fetch(:per_page)
      @params = attrs.fetch(:params)
    end

    def call
      params['log_date'] ? whois_report : whois_reports
    end

    private

    attr_reader :sort_by, :sort_order, :page, :per_page, :params

    def whois_report
      WhoisReport.where(log_date: params['log_date'])
    end

    def whois_reports
      WhoisReport.all.order(order).page(page).per(per_page)
    end

    def order
      { sort_by => sort_order }
    end
  end
end
