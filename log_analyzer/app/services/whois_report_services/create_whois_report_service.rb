# frozen_string_literal: true

module WhoisReportServices
  # log_analyzer/app/services/whois_report_services/create_whois_report_service.rb
  class CreateWhoisReportService
    include Service

    def initialize(attrs = {})
      @form = attrs.fetch(:form)
      @log = Rails.root.join(Settings.logs_path, form.log_date)
      @log_file = 'messages'
    end

    def call
      form.errors?
      return build_result(validation_error) unless form.valid?

      log_exist? ? process_log_data : raise_log_not_found
      build_result(create_whois)
    end

    private

    attr_reader :form, :log, :log_file

    def log_exist?
      File.directory? log.to_s
    end

    def process_log_data
      WhoisWorkers::WhoisWorker.perform_async(file_path, create_whois.id, form.log_date)
      create_whois.update_attributes(
        log_lines: count_log_lines,
        report_file_name: report_file_name,
        report_file_path: report_file_path
      )
    end

    def create_whois
      @create_whois ||= WhoisReport.create(form.to_hash)
    end

    def file_path
      "#{log}/#{log_file}"
    end

    def report_file_name
      "#{form.log_date}.csv"
    end

    def report_file_path
      Settings.csv_reports_path
    end

    def count_log_lines
      @count_log_lines ||= CountFileLines.proceed("#{log}/#{log_file}")
    end

    def raise_log_not_found
      raise Error::ValidationFailed, 'Invalid log date, resource not found'
    end
  end
end
