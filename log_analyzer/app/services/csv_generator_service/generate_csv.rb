# frozen_string_literal: true

require 'csv'

# log_analyzer/app/services/csv_generator_service
module CsvGeneratorService
  # log_analyzer/app/services/csv_generator_service/generate_csv.rb
  class GenerateCsv
    HEADERS = %w[SourceIP HardwareName DestinationIP DestinationName ConnectionTime].freeze
    OPTIONS = { col_sep: ',', write_headers: true }.freeze
    TMP_DIR = "#{Settings.csv_reports_path}/"

    def call(csv_data, file_name)
      filename = create_file(file_name)

      initialize_csv_file(filename)
      push_data_to_cvs_file(filename, csv_data)
    end

    private

    def initialize_csv_file(file_name)
      CSV.open(TMP_DIR + file_name, 'wb') do |row|
        row << HEADERS
      end
    end

    def push_data_to_cvs_file(file_name, csv_data)
      CSV.open(TMP_DIR + file_name, 'a+') do |row|
        csv_data.each do |csv_item|
          row << [
            csv_item[0],
            csv_item[1],
            csv_item[2],
            clear_string(csv_item[3]),
            csv_item[4]
          ]
        end

        row << [] << []
      end
    end

    def create_file(name)
      "#{name}.csv"
    end

    def clear_string(data)
      data.gsub(/[^a-zA-Z0-9\- ]/, '')
    end
  end
end
