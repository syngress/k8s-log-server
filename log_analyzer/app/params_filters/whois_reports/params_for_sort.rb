# frozen_string_literal: true

module WhoisReports
  # log_analyzer/app/params_filters/whois_reports/params_for_sort.rb
  class ParamsForSort
    extend ParamsFilter

    class << self
      def allowed_attributes
        %i[sort_by sort_order]
      end

      def allowed_values
        %i[id log_date]
      end
    end
  end
end
