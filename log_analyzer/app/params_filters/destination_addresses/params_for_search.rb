# frozen_string_literal: true

module DestinationAddresses
  # log_analyzer/app/params_filters/destination_addresses/params_for_search.rb
  class ParamsForSearch
    extend ParamsFilter
    class << self
      def allowed_attributes
        %i[id destination_ip]
      end
    end
  end
end
