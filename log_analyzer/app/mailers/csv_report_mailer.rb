# frozen_string_literal: true

# log_analyzer/app/mailers/csv_report_mailer.rb
class CsvReportMailer < ActionMailer::Base
  default from: Settings.mailer.default_from

  def send_report(email, execution_time, file_name)
    attachments['network_report.csv'] = File.read("#{Settings.csv_reports_path}/#{file_name}.csv")

    Rails.logger.info "== sending csv_report email to ==> #{email}"
    mail(to: email, subject: "Network Connection Report from #{file_name} took #{execution_time}")
  end
end
