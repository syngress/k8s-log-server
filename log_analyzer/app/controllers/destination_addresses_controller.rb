# frozen_string_literal: true

# app/controllers/destination_addresses_controller.rb
class DestinationAddressesController < ApplicationController
  before_action :destination_address, only: %i[show]
  include Pagination
  include Sorting

  def show
    render json: {
      serializer: DestinationAddressSerializers::DestinationAddressSerializer.new(
        destination_address
      ).serializable_hash
    }
  end

  def index
    setup_pagination(page: params[:page], per_page: params[:per_page])
    setup_sorting(sort_by: params[:sort_by], sort_order: params[:sort_order], params_for_sort: params_for_sort)
    render json: {
      recordsTotal: destination_addresses.count,
      restaurants: DestinationAddressSerializers::DestinationAddressSerializer.new(
        destination_addresses
      ).serializable_hash
    }
  end

  def create
    form = DestinationAddressForms::CreateDestinationAddressForm.new(destination_address_params)
    result = DestinationAddressServices::CreateDestinationAddressService.call(form: form)

    if result.error?
      render_error(result.object)
    else
      render json: DestinationAddressSerializers::DestinationAddressSerializer.new(result.object).serializable_hash.to_json
    end
  end

  private

  def destination_address
    @destination_address ||= DestinationAddress.find(params[:id])
  rescue ActiveRecord::RecordNotFound => e
    raise Error::ResourceNotFound.new(e)
  end

  def destination_addresses
    @destination_addresses ||= DestinationAddressServices::GetDestinationAddressService.call(
      page: page,
      per_page: per_page,
      sort_by: sort_by,
      sort_order: sort_order,
      params: params
    )
  end

  def destination_address_params
    params.permit(:destination_ip).to_h.symbolize_keys
  end

  def params_for_sort
    ::DestinationAddresses::ParamsForSort.allowed_values
  end
end
