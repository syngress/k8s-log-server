# frozen_string_literal: true

# # frozen_string_literal: true

json.extract! whois_report, :id, :ip, :whois, :created_at, :updated_at
json.url whois_report_url(whois_report, format: :json)
