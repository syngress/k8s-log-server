# frozen_string_literal: true

json.partial! 'whois_reports/whois_report', whois_report: @whois_report
