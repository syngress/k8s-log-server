# frozen_string_literal: true

# log_analyzer/app/workers/message_workers
module MessageWorkers
  # log_analyzer/app/workers/message_workers/email_worker.rb
  class EmailWorker
    include Sidekiq::Worker

    def perform(email, execution_time, file_name)
      CsvReportMailer.send_report(email, execution_time, file_name).deliver
    end
  end
end
