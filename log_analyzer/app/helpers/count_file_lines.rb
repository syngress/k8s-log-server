# frozen_string_literal: true

# app/helpers/count_file_lines.rb
module CountFileLines
  def self.proceed(file_path)
    File.read(file_path).each_line.count
  end
end
