# frozen_string_literal: true

# app/models/whois_report.rb
class WhoisReport < ApplicationRecord
  validates_presence_of :log_date
end
