# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/whois_reports', type: :request do
  let(:valid_attributes) do
    skip('Add a hash of attributes valid for your model')
  end

  let(:invalid_attributes) do
    skip('Add a hash of attributes invalid for your model')
  end

  let(:valid_headers) do
    {}
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      WhoisReport.create! valid_attributes
      get whois_reports_url, headers: valid_headers, as: :json
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      whois_report = WhoisReport.create! valid_attributes
      get whois_report_url(whois_report), as: :json
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new WhoisReport' do
        expect do
          post whois_reports_url,
               params: { whois_report: valid_attributes }, headers: valid_headers, as: :json
        end.to change(WhoisReport, :count).by(1)
      end

      it 'renders a JSON response with the new whois_report' do
        post whois_reports_url,
             params: { whois_report: valid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including('application/json'))
      end
    end

    context 'with invalid parameters' do
      it 'does not create a new WhoisReport' do
        expect do
          post whois_reports_url,
               params: { whois_report: invalid_attributes }, as: :json
        end.to change(WhoisReport, :count).by(0)
      end

      it 'renders a JSON response with errors for the new whois_report' do
        post whois_reports_url,
             params: { whois_report: invalid_attributes }, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to eq('application/json')
      end
    end
  end
end
